import React from "react";
import { Router, Route, browserHistory } from "react-router";
import Main from "./screens/Main.js";
import Alarm from "./screens/Alarm.js";
import Customize from "./screens/Customize.js";
import Countdown from "./screens/Countdown.js";
import Stopwatch from "./screens/Stopwatch.js";
import RepeatCountdown from "./screens/Repeat_Countdown.js";

const Routes = () => (
  <Router history={browserHistory} exact>
    <Route path="/" component={Main} />
    <Route path="/alarm" component={Alarm} />
    <Route path="/stopwatch" component={Stopwatch} />
    <Route path="/countdown" component={Countdown} />
    <Route path="/customize" component={Customize} />
    <Route path="/repeat-countdown" component={RepeatCountdown} />
  </Router>
);
export default Routes;
