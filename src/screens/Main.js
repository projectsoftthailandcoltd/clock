import React, { Component } from "react";
import CustomizButton from "../components/CustomizButton";
import Config from "../mobx/Config";
import "../App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: new Date().toLocaleTimeString("en-US", { hour12: false })
    };
  }

  componentDidMount = () => {
    this.clock = setInterval(() => this._setCurrentTime(), 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.clock);
  };

  _setCurrentTime = () => {
    this.setState({
      currentTime: new Date().toLocaleTimeString("en-US", { hour12: false })
    });
  };

  render() {
    let { currentTime } = this.state;
    return (
      <div
        className="App-container"
        style={{
          flexDirection: "column",
          backgroundImage: Config.getBackground
        }}
      >
        <div className="App-clock">{currentTime ? currentTime + " น." : null}</div>
        <a className="App-link" href="/countdown">
          Countdown Timer
        </a>
        <a className="App-link" href="/repeat-countdown">
          Repeat Countdown Timer
        </a>
        <a className="App-link" href="/stopwatch">
          Stopwatch
        </a>
        <a className="App-link" href="/alarm">
          Clock Alarm
        </a>
        <CustomizButton />
      </div>
    );
  }
}

export default App;
