import React, { Component } from "react";
import TimePicker from "rc-time-picker";
import { ToastsContainer, ToastsStore } from "react-toasts";
import { Row, Col } from "react-bootstrap";
import moment from "moment";
import swal from "sweetalert";
import Config from "../mobx/Config";
import { Header } from "../components/Header";
import BackButton from "../components/BackButton";
import ClockButton from "../components/ClockButton";

import "../App.css";
import "../styles/style.css";
import "rc-time-picker/assets/index.css";

var audio = new Audio();

const formattedSeconds = sec => {
  let hours = ("0" + (Math.floor(sec / 3600) % 24)).slice(-2);
  let minutes = ("0" + (Math.floor(sec / 60) % 60)).slice(-2);
  let seconds = ("0" + (sec % 60)).slice(-2);
  return hours + ":" + minutes + ":" + seconds;
};

class RepeatCountdownScreen extends Component {
  constructor(props) {
    super(props);

    this.timer = 0;
    this._setLap = this._setLap.bind(this);
    this._openBar = this._openBar.bind(this);
    this._setTime = this._setTime.bind(this);
    this._setDelay = this._setDelay.bind(this);
    this._closeBar = this._closeBar.bind(this);
    this._setReview = this._setReview.bind(this);
    this._handleStop = this._handleStop.bind(this);
    this._handleTime = this._handleTime.bind(this);
    this._handleInput = this._handleInput.bind(this);
    this._handleStart = this._handleStart.bind(this);
    this._handleReset = this._handleReset.bind(this);
    this._handleAlert = this._handleAlert.bind(this);
    this._handleReview = this._handleReview.bind(this);
    this._handleRestart = this._handleRestart.bind(this);

    this.state = {
      alert: false,
      openBar: true,
      running: false,
      setTime: "",
      totalTime: "",
      setReview: "",
      delay: 0,
      setLap: 0,
      seconds: 0,
      review: 0,
      setDelay: 0,
      remainLab: 0
    };
  }
  componentDidMount = () => {
    audio = new Audio(Config.getSound);
  };

  componentWillUnmount = () => {
    clearInterval(this.timer);
  };

  _openBar = () => {
    this.setState({ openBar: !this.state.openBar });
  };

  _closeBar = () => {
    this.setState({ openBar: false });
  };

  _setTime = () => {
    let { setTime } = this.state;
    if (setTime) {
      let HH = parseInt(setTime.slice(0, 2)) * 3600;
      let mm = parseInt(setTime.slice(3, 5)) * 60;
      let ss = parseInt(setTime.slice(6, 8));
      let seconds = HH + mm + ss;
      this.setState({ seconds: seconds });
    }
  };

  _setReview = () => {
    let { setReview } = this.state;
    let HH = parseInt(setReview.slice(0, 2)) * 3600;
    let mm = parseInt(setReview.slice(3, 5)) * 60;
    let ss = parseInt(setReview.slice(6, 8));
    let review = HH + mm + ss;
    this.setState({ review: review });
  };

  _setLap = () => {
    let { setLap } = this.state;
    let remainLab = parseInt(setLap);
    this.setState({ remainLab: remainLab });
  };

  _setDelay = () => {
    let { setDelay } = this.state;
    let delay = parseInt(setDelay);
    this.setState({ delay: delay });
  };

  _handleStart() {
    let { running, seconds, delay, review } = this.state;
    if (!running && seconds && this.state.remainLab !== 0) {
      this.setState({ running: true });
      this.timer = setInterval(() => {
        const newCount = this.state.seconds - 1;
        const HH = parseInt(this.state.setTime.slice(0, 2)) * 3600;
        const mm = parseInt(this.state.setTime.slice(3, 5)) * 60;
        const ss = parseInt(this.state.setTime.slice(6, 8));
        const setSeconds = HH + mm + ss;
        if (newCount > 0 && this.state.remainLab !== 0) {
          let totalTime = newCount + setSeconds * (this.state.remainLab - 1);
          this.setState({
            seconds: newCount,
            totalTime: totalTime
          });
          if (review) {
            if (newCount === review && this.state.remainLab === 1) {
              ToastsStore.success("กรุณาทบทวนก่อนหมดเวลา", review * 1000);
            }
          }
        } else if (newCount >= 0 && this.state.remainLab > 0) {
          const lab = this.state.remainLab - 1;
          this.setState({
            seconds: newCount,
            remainLab: lab
          });
          if (this.state.remainLab !== 0) {
            setTimeout(() => {
              this._setTime();
            }, delay * 1000);
          } else {
            clearInterval(this.timer);
            this.setState({ seconds: 0, alert: true, running: false });
            audio.play();
          }
        }
      }, 1000);
    } else if (this.state.remainLab === 0) {
      swal("ผิดพลาด!", "กรุณารุบะจำนวนรอบ", "warning");
    }
  }

  _handleStop() {
    if (this.timer) {
      clearInterval(this.timer);
      this.setState({ running: false });
    }
  }

  _handleReset() {
    this.setState({
      seconds: 0,
      setTime: "",
      setLap: 0,
      setDelay: 0,
      remainLab: 0,
      totalTime: "",
      delay: 0
    });
  }

  _handleRestart() {
    const { setTime, remainLab } = this.state;
    const HH = parseInt(setTime.slice(0, 2)) * 3600;
    const mm = parseInt(setTime.slice(3, 5)) * 60;
    const ss = parseInt(setTime.slice(6, 8));
    const setSeconds = HH + mm + ss;
    const totalTime = setSeconds * remainLab;

    this.setState({
      seconds: setSeconds,
      totalTime: totalTime
    });
  }

  _handleAlert = () => {
    this.setState({
      alert: !this.state.alert
    });
    audio.pause();
    audio.currentTime = 0;
  };

  _handleTime = event => {
    let value = event ? event.format("HH:mm:ss") : null;
    this.setState({ setTime: value });
  };

  _handleReview = event => {
    let value = event ? event.format("HH:mm:ss") : null;
    this.setState({ setReview: value });
  };

  _handleInput = event => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value });
  };

  render() {
    let { openBar, title, seconds, running, alert, setLap, remainLab, totalTime, delay } = this.state;
    return (
      <Row
        className="App-container"
        style={{
          backgroundImage: Config.getBackground
        }}
      >
        <BackButton />
        <ToastsContainer store={ToastsStore} position={"top_right"} lightBackground />
        <Col sm={9} className="click-div" onClick={this._closeBar}>
          <Header name={title} />
          <div className="countdown-timer">{formattedSeconds(seconds)}</div>
          {alert ? (
            <Button className="stop-btn" onClick={this._handleAlert}>
              หยุด
            </Button>
          ) : seconds === 0 ? null : (
            <div>
              {running === false || this.timer === 0 ? (
                <Button className="start-btn" onClick={this._handleStart}>
                  Start
                </Button>
              ) : (
                <Button className="stop-btn" onClick={this._handleStop}>
                  Stop
                </Button>
              )}
              {running === false && seconds !== 0 ? (
                <>
                  {running === false && this.state.setTime !== formattedSeconds(this.state.seconds) ? (
                    <Button className="stop-btn" onClick={this._handleRestart}>
                      Restart
                    </Button>
                  ) : null}
                  <Button className="stop-btn" onClick={this._handleReset}>
                    Reset
                  </Button>
                </>
              ) : null}
            </div>
          )}
        </Col>
        <CustomizBar
          open={openBar}
          state={this.state}
          _setLap={this._setLap}
          _setTime={this._setTime}
          _setDelay={this._setDelay}
          _setReview={this._setReview}
          _handleTime={this._handleTime}
          _handleInput={this._handleInput}
          _handleReview={this._handleReview}
        />
        <div className="bar-text">
          {totalTime ? `เวลาที่เหลือทั้งหมด ${formattedSeconds(totalTime)}` : null}
          {totalTime ? <br /> : null}
          {remainLab ? `จำนวนรอบที่เหลือ ${remainLab} จาก ${setLap} รอบ` : null}
          {remainLab ? <br /> : null}
          {delay ? `เวลาหน่วงระหว่างรอบ ${delay} วินาที` : null}
          {delay ? <br /> : null}
          {this.state.review ? `เวลาทบทวน ${this.state.setReview} ` : null}
        </div>
        <ClockButton _openBar={this._openBar} />
      </Row>
    );
  }
}

const CustomizBar = props =>
  props.open ? (
    <Col sm={3} className="bar">
      <div className="bar-header">ตั้งเวลานับถอยหลัง</div>
      <Row>
        <Col sm={8}>
          <TimePicker
            allowEmpty={false}
            placeholder="เวลา"
            onChange={props._handleTime}
            defaultOpenValue={moment("00:00:00")}
          />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setTime}>
            บันทึก
          </button>
        </Col>
      </Row>
      <div className="bar-header">จำนวนรอบ</div>
      <Row>
        <Col sm={8}>
          <input className="bar-input" type="number" name="setLap" placeholder="รอบ" onChange={props._handleInput} />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setLap}>
            บันทึก
          </button>
        </Col>
      </Row>
      <div className="bar-header">เวลาหน่วงระหว่างรอบ</div>
      <Row>
        <Col sm={8}>
          <input
            className="bar-input"
            type="number"
            name="setDelay"
            placeholder="วินาที"
            onChange={props._handleInput}
          />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setDelay}>
            บันทึก
          </button>
        </Col>
      </Row>
      <div className="bar-header">เวลาทบทวน</div>
      <Row>
        <Col sm={8}>
          <TimePicker
            allowEmpty={false}
            placeholder="เวลา"
            onChange={props._handleReview}
            defaultOpenValue={moment("00:00:00")}
          />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setReview}>
            บันทึก
          </button>
        </Col>
      </Row>
    </Col>
  ) : null;

const Button = props => <button type="button" {...props} className={"btn " + props.className} />;

export default RepeatCountdownScreen;
