import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Config from "../mobx/Config";
import { Header } from "../components/Header";
import BackButton from "../components/BackButton";

import "../styles/style.css";
import "../App.css";

const formattedSeconds = sec => {
  let hours = ("0" + (Math.floor(sec / 3600) % 24)).slice(-2);
  let minutes = ("0" + (Math.floor(sec / 60) % 60)).slice(-2);
  let seconds = ("0" + (sec % 60)).slice(-2);
  return hours + ":" + minutes + ":" + seconds;
};

const Button = props => <button type="button" {...props} className={"btn " + props.className} />;

class StopwatchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondsElapsed: 0,
      laps: [],
      lastClearedIncrementer: null
    };
    this.incrementer = null;
  }

  componentWillUnmount = () => {
    clearInterval(this.incrementer);
  };

  handleStartClick() {
    this.incrementer = setInterval(
      () =>
        this.setState({
          secondsElapsed: this.state.secondsElapsed + 1
        }),
      1000
    );
  }

  handleStopClick() {
    clearInterval(this.incrementer);
    this.setState({
      lastClearedIncrementer: this.incrementer
    });
  }

  handleResetClick() {
    clearInterval(this.incrementer);
    this.setState({
      secondsElapsed: 0,
      laps: []
    });
  }

  handleLabClick() {
    this.setState({
      laps: this.state.laps.concat([this.state.secondsElapsed])
    });
  }

  render() {
    let { secondsElapsed, lastClearedIncrementer, laps } = this.state;
    return (
      <Row
        className="App-container"
        style={{
          backgroundImage: Config.getBackground
        }}
      >
        <BackButton />
        <Header name={"Stopwatch"} />
        <Col className="text-center" sm={6}>
          <h1 className="stopwatch-timer">{formattedSeconds(secondsElapsed)}</h1>
          {secondsElapsed === 0 || this.incrementer === lastClearedIncrementer ? (
            <Button className="start-btn" onClick={this.handleStartClick.bind(this)}>
              Start
            </Button>
          ) : (
            <Button className="stop-btn" onClick={this.handleStopClick.bind(this)}>
              Stop
            </Button>
          )}
          {secondsElapsed !== 0 && this.incrementer !== lastClearedIncrementer ? (
            <Button onClick={this.handleLabClick.bind(this)}>Lab</Button>
          ) : null}
          {secondsElapsed !== 0 && this.incrementer === lastClearedIncrementer ? (
            <Button className="stop-btn" onClick={this.handleResetClick.bind(this)}>
              Reset
            </Button>
          ) : null}
        </Col>
        <Col className="stopwatch-column" sm={6}>
          <ul className="stopwatch-laps">
            {laps.map((lap, i) => (
              <li className="stopwatch-lap">
                <strong>{i + 1}</strong>| {formattedSeconds(lap)}
              </li>
            ))}
          </ul>
        </Col>
      </Row>
    );
  }
}

export default StopwatchScreen;
