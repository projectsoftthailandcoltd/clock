import React, { Component } from "react";
import TimePicker from "rc-time-picker";
import { Row, Col } from "react-bootstrap";
import Config from "../mobx/Config";
import { Header } from "../components/Header";
import BackButton from "../components/BackButton";
import ClockButton from "../components/ClockButton";

import "../App.css";
import "../styles/style.css";
import "rc-time-picker/assets/index.css";

var audio = new Audio();
var beep = new Audio();

function decTimes(startTime, endTime) {
  var times = [0, 0, 0];
  var max = times.length;

  var a = (startTime || "").split(":");
  var b = (endTime || "").split(":");

  // normalize time values
  for (var i1 = 0; i1 < max; i1++) {
    a[i1] = isNaN(parseInt(a[i1])) ? 0 : parseInt(a[i1]);
    b[i1] = isNaN(parseInt(b[i1])) ? 0 : parseInt(b[i1]);
  }

  // store time values
  for (var i2 = 0; i2 < max; i2++) {
    times[i2] = a[i2] - b[i2];
  }

  var hours = times[0];
  var minutes = times[1];
  var seconds = times[2];

  if (a[2] < b[2]) {
    minutes -= 1;
  }

  if (a[1] < b[1]) {
    hours -= 1;
  }

  return ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2) + ":" + ("0" + seconds).slice(-2);
}

class AlarmScreen extends Component {
  constructor(props) {
    super(props);

    this._openBar = this._openBar.bind(this);
    this._setAlarm = this._setAlarm.bind(this);
    this._handleInput = this._handleInput.bind(this);
    this._setAlarmBefore = this._setAlarmBefore.bind(this);
    this.state = {
      openBar: true,
      alert: false,
      currentTime: new Date().toLocaleTimeString("en-US", { hour12: false }),
      setTime: "",
      alarmTime: "",
      alertBefore: false
    };
  }

  componentDidMount = () => {
    this.clock = setInterval(() => this._setCurrentTime(), 1000);
    this.interval = setInterval(() => this._checkAlarmClock(), 1000);
    beep = new Audio(Config.getSound);
    audio = new Audio(Config.getSound);
  };

  componentWillUnmount = () => {
    clearInterval(this.interval);
  };

  _openBar = () => {
    this.setState({ openBar: !this.state.openBar });
  };

  _closeBar = () => {
    this.setState({ openBar: false });
  };

  _setCurrentTime = () => {
    this.setState({
      currentTime: new Date().toLocaleTimeString("en-US", { hour12: false })
    });
  };

  _handleInput = event => {
    let value = event ? event.format("HH:mm:ss") : null;
    this.setState({ setTime: value });
  };

  _setAlarm = () => {
    let { setTime } = this.state;
    this.setState({ alarmTime: setTime });
  };

  _setAlarmBefore = () => {
    let { alertBefore } = this.state;
    this.setState({ alertBefore: !alertBefore });
  };

  _checkAlarmClock = () => {
    if (this.state.alarmTime !== "undefined" || this.state.alarmTime) {
      if (this.state.alertBefore ? this.state.currentTime === decTimes(this.state.alarmTime, "00:05:00") : false) {
        this.setState({
          alert: true
        });
        audio.play();
      } else if (this.state.currentTime === this.state.alarmTime) {
        beep.play();
      }
    }
  };

  _toggleAlert = () => {
    this.setState({
      alert: !this.state.alert,
      alarmTime: ""
    });
    audio.pause();
    audio.currentTime = 0;
  };

  render() {
    let { currentTime, openBar, alert, alarmTime } = this.state;
    return (
      <Row
        className="App-container"
        style={{
          backgroundImage: Config.getBackground
        }}
      >
        <BackButton />
        <Col sm={9} className="click-div" onClick={this._closeBar}>
          <Header name={"Clock Alarm"} />
          <div className="clock">{currentTime ? currentTime + " น." : null}</div>
          {alert === true ? (
            <Button className="stop-btn" onClick={this._toggleAlert}>
              หยุด
            </Button>
          ) : null}
        </Col>
        <CustomizBar
          state={this.state}
          open={openBar}
          _setAlarm={this._setAlarm}
          _handleInput={this._handleInput}
          _setAlarmBefore={this._setAlarmBefore}
        />
        <div className="bar-text">เวลาแจ้งเตือน {alarmTime ? alarmTime + " น." : "ยังไม่ตั้งเวลา"}</div>
        <ClockButton _openBar={this._openBar} />
      </Row>
    );
  }
}

const CustomizBar = props =>
  props.open ? (
    <Col sm={3} className="bar">
      <div className="bar-header">ตั้งเวลาแจ้งเตือน</div>
      <Row>
        <Col sm={8}>
          <TimePicker allowEmpty={false} placeholder="เวลา" onChange={props._handleInput} />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setAlarm}>
            บันทึก
          </button>
        </Col>
        <Col sm={12} style={{ marginTop: "1vw" }}>
          <input
            type="checkbox"
            name="alertBefore"
            checked={props.state.alertBefore}
            onChange={props._setAlarmBefore}
          />
          <label style={{ fontSize: "1.3vw", marginLeft: "1vw" }}>เตือนก่อนหมดเวลา 5 นาที</label>
        </Col>
      </Row>
    </Col>
  ) : null;

const Button = props => <button type="button" {...props} className={"btn " + props.className} />;

export default AlarmScreen;
