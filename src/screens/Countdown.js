import React, { Component } from "react";
import TimePicker from "rc-time-picker";
import { Row, Col } from "react-bootstrap";
import moment from "moment";
import Config from "../mobx/Config";
import { Header } from "../components/Header";
import BackButton from "../components/BackButton";
import ClockButton from "../components/ClockButton";

import "../App.css";
import "../styles/style.css";
import "rc-time-picker/assets/index.css";

var audio = new Audio();

const formattedSeconds = sec => {
  let hours = ("0" + (Math.floor(sec / 3600) % 24)).slice(-2);
  let minutes = ("0" + (Math.floor(sec / 60) % 60)).slice(-2);
  let seconds = ("0" + (sec % 60)).slice(-2);
  return hours + ":" + minutes + ":" + seconds;
};

class CountdownScreen extends Component {
  constructor(props) {
    super(props);

    this.timer = 0;
    this._openBar = this._openBar.bind(this);
    this._setTime = this._setTime.bind(this);
    this._closeBar = this._closeBar.bind(this);
    this._handleStop = this._handleStop.bind(this);
    this._handleTime = this._handleTime.bind(this);
    this._handleStart = this._handleStart.bind(this);
    this._handleReset = this._handleReset.bind(this);
    this._handleAlert = this._handleAlert.bind(this);

    this.state = {
      openBar: true,
      running: false,
      alert: false,
      title: "",
      setTitle: "",
      setTime: "",
      seconds: 0
    };
  }
  componentDidMount = () => {
    audio = new Audio(Config.getSound);
  };

  componentWillUnmount = () => {
    clearInterval(this.timer);
  };

  _openBar = () => {
    this.setState({ openBar: !this.state.openBar });
  };

  _closeBar = () => {
    this.setState({ openBar: false });
  };

  _setTime = () => {
    let { setTime } = this.state;
    if (setTime) {
      let HH = parseInt(setTime.slice(0, 2)) * 3600;
      let mm = parseInt(setTime.slice(3, 5)) * 60;
      let ss = parseInt(setTime.slice(6, 8));
      let seconds = HH + mm + ss;
      this.setState({ seconds: seconds });
    }
  };

  _setTitle = () => {
    let { setTitle } = this.state;
    let title = setTitle;
    this.setState({ title: title });
  };

  _handleStart() {
    let { running, seconds } = this.state;
    if (!running && seconds) {
      this.setState({ running: true });
      this.timer = setInterval(() => {
        const newCount = this.state.seconds - 1;
        if (newCount > 0) {
          this.setState({ seconds: newCount });
        } else {
          clearInterval(this.timer);
          this.setState({ seconds: 0, alert: true, running: false });
          audio.play();
        }
      }, 1000);
    }
  }

  _handleStop() {
    if (this.timer) {
      clearInterval(this.timer);
      this.setState({ running: false });
    }
  }

  _handleReset() {
    this.setState({ seconds: 0 });
  }

  _handleAlert = () => {
    this.setState({
      alert: !this.state.alert
    });
    audio.pause();
    audio.currentTime = 0;
  };

  _handleTime = event => {
    let value = event ? event.format("HH:mm:ss") : null;
    this.setState({ setTime: value });
  };

  _handleInput = event => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value });
  };

  render() {
    let { openBar, title, seconds, running, alert } = this.state;
    return (
      <Row
        className="App-container"
        style={{
          backgroundImage: Config.getBackground
        }}
      >
        <BackButton />
        <Col sm={9} className="click-div" onClick={this._closeBar}>
          <Header name={title} />
          <div className="countdown-timer">{formattedSeconds(seconds)}</div>
          {alert ? (
            <Button className="stop-btn" onClick={this._handleAlert}>
              หยุด
            </Button>
          ) : (
            <div>
              {running === false || this.timer === 0 ? (
                <Button className="start-btn" onClick={this._handleStart}>
                  Start
                </Button>
              ) : (
                <Button className="stop-btn" onClick={this._handleStop}>
                  Stop
                </Button>
              )}
              {running === false && seconds !== 0 ? (
                <Button className="stop-btn" onClick={this._handleReset}>
                  Reset
                </Button>
              ) : null}
            </div>
          )}
        </Col>
        <CustomizBar
          state={this.state}
          open={openBar}
          _handleInput={this._handleInput}
          _handleTime={this._handleTime}
          _setTime={this._setTime}
          _setTitle={this._setTitle}
        />
        <ClockButton _openBar={this._openBar} />
      </Row>
    );
  }
}

const CustomizBar = props =>
  props.open ? (
    <Col sm={3} className="bar">
      <div className="bar-header">ชื่อหัวข้อ</div>
      <Row>
        <Col sm={8}>
          <input className="bar-input" type="text" name="setTitle" placeholder="ชื่อ" onChange={props._handleInput} />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setTitle}>
            บันทึก
          </button>
        </Col>
      </Row>
      <div className="bar-header">ตั้งเวลานับถอยหลัง</div>
      <Row>
        <Col sm={8}>
          <TimePicker
            allowEmpty={false}
            placeholder="เวลา"
            onChange={props._handleTime}
            defaultOpenValue={moment("00:00:00")}
          />
        </Col>
        <Col sm={4}>
          <button className="bar-btn" onClick={props._setTime}>
            บันทึก
          </button>
        </Col>
      </Row>
    </Col>
  ) : null;

const Button = props => <button type="button" {...props} className={"btn " + props.className} />;

export default CountdownScreen;
