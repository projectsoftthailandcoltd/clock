import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Config from "../mobx/Config";
import { HeaderLeft } from "../components/Header";
import BackButton from "../components/BackButton";

import playButton from "../assets/icons/play_button.png";
import bg1 from "../assets/backgrounds/bg_1.jpg";
import bg2 from "../assets/backgrounds/bg_2.jpg";
import bg3 from "../assets/backgrounds/bg_3.jpg";
import bg4 from "../assets/backgrounds/bg_4.jpg";
import bg5 from "../assets/backgrounds/bg_5.jpg";

import sound1 from "../assets/audios/sound_1.mp3";
import sound2 from "../assets/audios/sound_2.mp3";
import sound3 from "../assets/audios/sound_3.mp3";
import sound4 from "../assets/audios/sound_4.mp3";
import sound5 from "../assets/audios/sound_5.mp3";

import "../styles/style.css";
import "../App.css";

const imageData = [bg1, bg2, bg3, bg4, bg5];
const audioData = [sound1, sound2, sound3, sound4, sound5];
var buff = [];

class CustomizeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      render: false,
      currentBG: Config.background.replace(".jpg", ""),
      currentSound: Config.sound.replace(".mp3", ""),
      currentSoundID: null
    };
  }

  setBG = e => {
    let value = e.target.value;
    let indexOf = value.replace("/static/media/", "").indexOf(".");
    let name = value.replace("/static/media/", "").substring(0, indexOf);
    Config.setBackground(name + ".jpg");
    this.setState({ render: true, currentBG: name });
  };

  setSound = e => {
    let value = e.target.value;
    let indexOf = value.replace("/static/media/", "").indexOf(".");
    let name = value.replace("/static/media/", "").substring(0, indexOf);
    Config.setSound(name + ".mp3");
    this.setState({ render: true, currentSound: name });
  };

  playSound = (audio, id) => {
    let { currentSoundID } = this.state;
    if (currentSoundID !== id) {
      buff.push(audio);
      buff[0].src = audioData[id];
      buff[0].loop = false;
      buff[0].play();
      this.setState({ currentSoundID: id });
    } else {
      buff[0].pause();
      buff[0].currentTime = 0;
      buff.pop();
      this.setState({ currentSoundID: null });
    }
  };

  render() {
    let { currentBG, currentSound } = this.state;
    var audio = new Audio(Config.getSound);

    return (
      <Row className="App-container" style={{ flexDirection: "column", backgroundImage: Config.getBackground }}>
        <BackButton />
        <HeaderLeft name={"ตั้งค่า"} />
        <h1>เลือกภาพพื้นหลัง</h1>
        <Row className="background-select">
          {imageData.map((el, i) => (
            <Col m={i === 0 ? { span: 2, offset: 1 } : 2} key={i}>
              <label>
                <input
                  type="radio"
                  name="name"
                  value={el}
                  onClick={this.setBG}
                  checked={el.indexOf(currentBG) !== -1 ? true : false}
                />
                <img alt="background" className="background-img" src={el} />
              </label>
            </Col>
          ))}
        </Row>
        <h1>เลือกเสียงแจ้งเตือน</h1>
        <Row className="sound-select">
          {audioData.map((el, i) => (
            <Col sm={i === 0 ? { span: 2, offset: 1 } : 2} key={i}>
              <label>
                <input
                  type="radio"
                  name="sound"
                  value={el}
                  onClick={event => {
                    this.setSound(event);
                    this.playSound(audio, i);
                  }}
                  checked={el.indexOf(currentSound) !== -1 ? true : false}
                />
                <img alt="sound" className="sound-img" src={playButton} />
                <div>
                  {el.replace("/static/media/", "").substring(0, el.replace("/static/media/", "").indexOf("."))}
                </div>
              </label>
            </Col>
          ))}
        </Row>
      </Row>
    );
  }
}

export default CustomizeScreen;
