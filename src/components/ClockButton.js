import React from "react";
import clock from "../assets/icons/clock.png";
import "../App.css";

const ClockButton = props => (
  <div className="App-customize" onClick={props._openBar}>
    <img alt="btn" src={clock} style={{ width: "3vw", height: "3vw" }} />
  </div>
);

export default ClockButton;
