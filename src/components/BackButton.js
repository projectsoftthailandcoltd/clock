import React from "react";
import back from "../assets/icons/back_white.png";
import "../App.css";

const BackButton = () => (
  <a className="App-back" href="/">
    <img alt="btn" src={back} style={{ width: "3vw", height: "3vw" }} />
  </a>
);

export default BackButton;
