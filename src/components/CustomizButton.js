import React from "react";
import gear from "../assets/icons/gear.png";
import "../App.css";

const CustomizButton = () => (
  <a className="App-customize" href="/customize">
    <img alt="btn" src={gear} style={{ width: "3vw", height: "3vw" }} />
  </a>
);

export default CustomizButton;
