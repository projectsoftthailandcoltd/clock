import React from "react";
import "../App.css";

export const Header = props => <h2 className="header">{props.name}</h2>;
export const HeaderLeft = props => <h2 className="header-left">{props.name}</h2>;

