import { action, computed, observable } from "mobx";

class Config {
  @observable sound = "sound_1.mp3";
  @observable background = "bg_1.jpg";

  constructor() {
    let soundLocal = localStorage.getItem("Sound");
    let backgroundLocal = localStorage.getItem("Background");
    this.sound = soundLocal ? soundLocal : this.sound;
    this.background = backgroundLocal ? backgroundLocal : this.background;
  }

  @action setSound(name) {
    this.sound = name;
    localStorage.setItem("Sound", name);
  }

  @action setBackground(name) {
    this.background = name;
    localStorage.setItem("Background", name);
  }

  @computed
  get getSound() {
    let sound = require(`../assets/audios/${this.sound}`);
    return sound;
  }

  @computed
  get getBackground() {
    let bg = require(`../assets/backgrounds/${this.background}`);
    return `url(${bg})`;
  }
}

export default new Config();
